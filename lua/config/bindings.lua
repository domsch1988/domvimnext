local wk = require("which-key")
local map = vim.api.nvim_set_keymap

map('n', '<C-A-H>', '<cmd>wincmd h<cr>', { silent = true })
map('n', '<C-A-J>', '<cmd>wincmd j<cr>', { silent = true })
map('n', '<C-A-K>', '<cmd>wincmd k<cr>', { silent = true })
map('n', '<C-A-L>', '<cmd>wincmd l<cr>', { silent = true })

wk.register({
  f = {
    name = "file",
    f = { "<cmd>Telescope find_files<cr>", "Find File" },
    c = { "<cmd>Telescope colorscheme<cr>", "Change Colorscheme" },
    a = { "<cmd>Telescope buffers<cr>", "Change Buffer" },
    s = { "<cmd>Telescope live_grep<cr>", "Live Grep" },
    g = { "<cmd>Telescope current_buffer_fuzzy_find<cr>", "Find in File" },
  },
  g = {
    name = "git",
    g = { "<cmd>LazyGit<cr>", "Find File" },
    l = { "<cmd>:Gitsigns toggle_current_line_blame<cr>", "Toggle Line Blame" }
  },
  e = { "<cmd>Neotree<cr>", "Neotree" },
  x = { "<cmd>BufDel<cr>", "Close Buffer" },
  p = { "<cmd>BufferLineCycleNext<cr>", "Next Buffer" },
  u = { "<cmd>BufferLineCyclePrev<cr>", "Previous Buffer" },
  q = { "<cmd>quitall<cr>", "Quit Nvim" },
  b = {
    name = "buffer",
    f = { "<cmd>lua vim.lsp.buf.format()<cr>", "Autoformat Buffer" },
    c = { "<cmd>BufferLinePick<cr>", "Choose Buffer" },
  },
  a = {
    name = "Random",
  },
  s = {
    name = "Save",
    a = { "<cmd>w<cr>", "Save Changes" },
    s = { "<cmd>SessionManager save_current_session<cr>", "Save Session" },
    l = { "<cmd>SessionManager load_last_session<cr>", "Load Session" },
    f = { "<cmd>SessionManager load_session<cr>", "Find Session" }
  },
  i = {
    name = "Interface",
    f = { "<cmd>set background=dark<cr>", "Dark Mode" },
    d = { "<cmd>set background=light<cr>", "Light Mode" },
    b = { "<cmd>Neotree buffers<cr>", "Show Buffer Sidebar" },
  },
}, { prefix = "<leader>" })
