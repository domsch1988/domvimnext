return {
  "sidebar-nvim/sidebar.nvim",
  config = function()
    local session_utils = require("session_manager.utils")
    local Loclist = require("sidebar-nvim.components.loclist")
    local sessions = session_utils.get_sessions()
    local dirs = {}
    local filenames = {}

    for _, file, _ in pairs(sessions) do
      local directory = session_utils.shorten_path(file.dir)
      table.insert(dirs, directory)
      table.insert(filenames, file.filename)
    end

    function MagicLines(s)
        if s:sub(-1)~="\n" then s=s.."\n" end
        return s:gmatch("(.-)\n")
    end

    local function get_git_root()
        -- Check if there are any open buffers
        if vim.fn.bufexists(0) == 0 then
            return nil
        end

        -- Get the current buffer's file path
        local file_path = vim.fn.expand('%:p')

        -- Check if the buffer is associated with a file
        if file_path == '' or vim.fn.filereadable(file_path) == 0 then
            return nil
        end

        -- Function to check if a directory contains a Git repository
        local function is_git_repo(dir)
            local git_dir = dir .. '/.git'
            return vim.fn.isdirectory(git_dir) == 1
        end

        -- Traverse upwards in the directory hierarchy to find the Git root
        local current_dir = file_path
        repeat
            if is_git_repo(current_dir) then
                return current_dir
            end
            current_dir = vim.fn.fnamemodify(current_dir, ':h')
        until current_dir == ''

        return nil -- Not in a Git repository or encountered an error
    end

    local function gitlog(file)
      local folder = ""
      local git_root = get_git_root()
        if git_root then
           folder = git_root
        else
          folder = "D:/Development/ansible-config"
        end
      local command = "git -C " .. folder .. " log -n 10 --pretty=format:\"%ad ; %s ; %an\" --no-merges --date=relative " .. file
        local result = vim.fn.system(command)
        return(result)
      end

    local loclist = Loclist:new({
        group_icon = { closed = "", opened = "" },
        -- badge showing the number of items in each group
        show_group_count = true,
        -- if empty groups should be displayed
        show_empty_groups = true,
        -- if there's a single group, skip rendering the group controls
        omit_single_group = false,
        -- initial state of the groups
        groups_initially_closed = false,
        -- highlight groups for each control element
        highlights = {
            group = "SidebarNvimLabel",
            group_count = "SidebarNvimSectionTitle",
        },
    })

    local section = {
      title = "Sessions",
      icon = " ",
      draw = function()
        return dirs
      end,
      bindings = {
        ["l"] = function(line, _)
          vim.cmd("SidebarNvimClose")
          local filename = filenames[line + 1]
          session_utils.load_session(filename, false)
        end,
      }
    }

    local section_githist = {
      title = "File History",
      icon = "H ",
      draw = function(ctx)
        local current_file = vim.api.nvim_buf_get_name(0)
        local filename = gitlog(current_file)
        if string.find(filename, "ago") then
          local lines = {}
          for line in string.gmatch(filename, "[^\r\n]+") do
              local elements = {}
              for element in string.gmatch(line, "[^;]+") do
                  table.insert(elements, element)
              end
              table.insert(lines, elements)
          end

          loclist:clear({
            remove_groups = true,
          })

          for i, line_elements in ipairs(lines) do
              loclist:add_item({
                  group = line_elements[2],
                  lnum = i,
                  col = 2,
                  left = {
                      { text = line_elements[3], hl = "MyHighlightGroup" }
                  },
                  right = {
                    { text = line_elements[1], hl = "MyHighlightGroup"}
                  },
                  order = 1
              })
          end
        else
          loclist:clear({
            remove_groups = true,
          })

          loclist:add_item({
              group = "empty",
              lnum = 1,
              col = 2,
              left = {
                  { text = "", hl = "MyHighlightGroup" }
              },
              right = {
                { text = "", hl = "MyHighlightGroup"}
              },
              order = 1
          })
        end

        local lines, hl = {}, {}

        table.insert(lines, "Commits for the File:")

        loclist:draw(ctx, lines, hl)

        return { lines = lines, hl = hl }
          end,
        }

    require("sidebar-nvim").setup({
      open = true,
      side = "right",
      initial_width = 55,
      section_separator = { "______________________" },
      section_title_separator = {  },
      datetime = {
        icon = " ",
        format = "%H:%M"
      },
      sections = {
        section,
        "git",
        "todos",
        "diagnostics",
        section_githist
      },
    })
  end
}
