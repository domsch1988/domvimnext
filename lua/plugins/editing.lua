return {
  {
    "echasnovski/mini.nvim",
    config = function()
      require("mini.pairs").setup()
      require("mini.comment").setup()
      require("mini.move").setup()
      require("mini.starter").setup({
        autoopen = true,
        header = "DDDDDDDDDDDDD                                              VVVVVVVV           VVVVVVVV iiii                          \nD::::::::::::DDD                                           V::::::V           V::::::Vi::::i                         \nD:::::::::::::::DD                                         V::::::V           V::::::V iiii                          \nDDD:::::DDDDD:::::D                                        V::::::V           V::::::V                               \n  D:::::D    D:::::D    ooooooooooo      mmmmmmm    mmmmmmm V:::::V           V:::::Viiiiiii    mmmmmmm    mmmmmmm   \n  D:::::D     D:::::D oo:::::::::::oo  mm:::::::m  m:::::::mmV:::::V         V:::::V i:::::i  mm:::::::m  m:::::::mm \n  D:::::D     D:::::Do:::::::::::::::om::::::::::mm::::::::::mV:::::V       V:::::V   i::::i m::::::::::mm::::::::::m\n  D:::::D     D:::::Do:::::ooooo:::::om::::::::::::::::::::::m V:::::V     V:::::V    i::::i m::::::::::::::::::::::m\n  D:::::D     D:::::Do::::o     o::::om:::::mmm::::::mmm:::::m  V:::::V   V:::::V     i::::i m:::::mmm::::::mmm:::::m\n  D:::::D     D:::::Do::::o     o::::om::::m   m::::m   m::::m   V:::::V V:::::V      i::::i m::::m   m::::m   m::::m\n  D:::::D     D:::::Do::::o     o::::om::::m   m::::m   m::::m    V:::::V:::::V       i::::i m::::m   m::::m   m::::m\n  D:::::D    D:::::D o::::o     o::::om::::m   m::::m   m::::m     V:::::::::V        i::::i m::::m   m::::m   m::::m\nDDD:::::DDDDD:::::D  o:::::ooooo:::::om::::m   m::::m   m::::m      V:::::::V        i::::::im::::m   m::::m   m::::m\nD:::::::::::::::DD   o:::::::::::::::om::::m   m::::m   m::::m       V:::::V         i::::::im::::m   m::::m   m::::m\nD::::::::::::DDD      oo:::::::::::oo m::::m   m::::m   m::::m        V:::V          i::::::im::::m   m::::m   m::::m\nDDDDDDDDDDDDD           ooooooooooo   mmmmmm   mmmmmm   mmmmmm         VVV           iiiiiiiimmmmmm   mmmmmm   mmmmmm\n"
      })
    end
  },
  -- {
  --   "uga-rosa/ccc.nvim",
  --   config =function ()
  --     vim.cmd('CccHighlighterEnable')
  --   end
  -- }
}
