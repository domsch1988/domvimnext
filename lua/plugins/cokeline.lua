return {
  {
    "willothy/nvim-cokeline",
    config = function()
      require('cokeline').setup({
        components = {
          {
            text = function(buffer)
              return
                  ' ' .. buffer.devicon.icon
            end,
            fg = function(buffer)
              return
                  buffer.devicon.color
            end,
            style = function(_)
              return
                  nil
            end,
            truncation = { priority = 1 }
          },
          {
            text = function(buffer) return ' ' .. buffer.filename .. ' ' end,
          },
          {
            text = '󰅙 ',
            delete_buffer_on_left_click = true,
          },
          {
            text = ' ',
          },
        },
        sidebar = {
          filetype = 'neo-tree',
          components = {
            {
              text = 'DomVimNEXT',
              fg = vim.g.terminal_color_1,
              bg = vim.g.terminal_color_0,
              style = 'italic',
            }
          },
        },
      })
    end
  },
}
