# DomVimNext

![DomVimNext](domvimnext.png)

This is my personal Configuration for NeoVim.
The main point is, that i don't do a lot of "classic Programming" work. Most of what i do is Navigating Files, Reading Changes others made, Looking stuff up and making minor changes.
Because of that, this Configuration will lack a few things larger other Distributions might have. On the flipside i will try to set this up to make some non-programming related work easier.
- 📁 Working with Folders, Files and Repositories should be as easy or easier than the OSes Filebrowser
- 🔍 It should be easy and fast to get to the file and position that you want

# Dependencies

## Windows

- General
    - Lazygit
        - Lazygit is a "soft" Requirement. We do have TimUntersberger/neogit to do all Git work. Lazygit is a bit more "visual". Both have their Respective Keybindings
- For LSP Installs
    - Node.js / npm
- For Treesitter
    - Any C Compiler. MinGW is recommended https://www.mingw-w64.org/
